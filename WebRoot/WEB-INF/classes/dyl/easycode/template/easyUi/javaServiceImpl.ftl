<#macro selectolums><#list table.columns as c>${c.columnName}<#if c_has_next>,</#if></#list></#macro>
<#macro insertFlag><#list table.baseColumns as c><#if c_has_next>?,<#else>?</#if></#list></#macro>
<#macro updateColums><#list table.baseColumns as c><#if c_has_next>${c.columnName}=?,<#else>${c.columnName}=?</#if></#list></#macro>
<#macro updateVals><#list table.baseColumns as c><#if c_has_next>${table.javaProperty}.get${c.javaPropertyForGetSet}(),<#else>${table.javaProperty}.get${c.javaPropertyForGetSet}()</#if></#list></#macro>
package ${table.packageName};

import java.util.List;
import java.util.ArrayList;
<#list table.importList as i>
import ${i};
</#list>
import com.common.util.BeanUtil;
import com.common.page.Page;
import com.common.page.DAOHelper;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import com.common.page.DAOHelper;
import javax.annotation.Resource;
/**

 * @author Dyl
 * ${sysTime}
 */
@Service
public class ${table.className}ServiceImpl {
	<#--private static Log log = LogFactory.getLog(${table.className}DaoImpl.class);-->
	@Resource
	private JdbcTemplate jdbcTemplate;
	@Resource
	private DAOHelper daoHelper;
	/**
	 * 说明：分页查询表${table.tableName}记录封装成List集合
	 * @return JSONObject
	 */
	public JSONObject  find${table.className}List(Page page,${table.className} ${table.javaProperty}) throws Exception{
		String sql = "select <@selectolums/> from ${table.tableName} t where 1=1";
		List<Object> con = new ArrayList<Object>();
		<#list table.columns as c>
		<#if c.javaType =='String'>
		if(StringUtils.isNotEmpty(${table.javaProperty}.get${c.javaPropertyForGetSet}())){
			sql+=" and t.${c.columnName} like ?";
			con.add("%"+${table.javaProperty}.get${c.javaPropertyForGetSet}()+"%");
		}
		<#else>
		if(${table.javaProperty}.get${c.javaPropertyForGetSet}()!=null){
			sql+=" and t.${c.columnName}=?";
			con.add(${table.javaProperty}.get${c.javaPropertyForGetSet}());
		}
		</#if>
		</#list>
		List<${table.className}> ${table.javaProperty}List =  BeanUtil.changeListMapToListBean(daoHelper.queryPageInfoListBySpringSql(sql,page,con),${table.className}.class);
		JSONObject json = new JSONObject();
		json.put("rows",${table.javaProperty}List);
		json.put("total", page.getTotalCount());
		return json;
	}
	/**
	 * 说明：根据主键查询表${table.tableName}中的一条记录 
	 * @return ${table.className}
	 */
	public ${table.className} get${table.className}(${table.primaryKey.javaType}  ${table.primaryKey.javaProperty}) throws Exception{
		String sql = "select <@selectolums/> from ${table.tableName} where ${table.primaryKey.columnName}=?";
		${table.className} ${table.javaProperty}  = (${table.className}) BeanUtil.changeMapToBean(jdbcTemplate.queryForMap(sql, new Object[]{${table.primaryKey.javaProperty}}),${table.className}.class); 
		return ${table.javaProperty};
	}
	<#if hasEdit??>
	/**
	 * 说明：往表${table.tableName}中插入一条记录
	 * @return int >0代表操作成功
	 */
	public int insert${table.className}(${table.className} ${table.javaProperty}) throws Exception{
		String sql = "insert into ${table.tableName}(<@selectolums/>) values(sys_guid(),<@insertFlag/>)";
		int returnVal=jdbcTemplate.update(sql,new Object[]{<#list table.baseColumns as c>${table.javaProperty}.get${c.javaPropertyForGetSet}()<#if c_has_next>,</#if></#list>});
		return returnVal;
	}
	/**
	 * 说明：根据主键更新表${table.tableName}中的记录
	 * @return int >0代表操作成功
	 */
	public int update${table.className}(${table.className} ${table.javaProperty}) throws Exception{
		String sql = "update ${table.tableName} t set ";
		List<Object> con = new ArrayList<Object>();
		<#list table.baseColumns as c>
			if(${table.javaProperty}.get${c.javaPropertyForGetSet}()!=null){
				sql+="t.${c.columnName}=?,";
				con.add(${table.javaProperty}.get${c.javaPropertyForGetSet}());
			}
		</#list>
		sql=sql.substring(0,sql.length()-1);
		sql+=" where ${table.primaryKey.columnName}=?";
		con.add(${table.javaProperty}.get${table.primaryKey.javaPropertyForGetSet}());
		int returnVal=jdbcTemplate.update(sql,con.toArray());
		return returnVal;
	}
	<#--
	/**
	 * 说明：根据主键更新表${table.tableName}中的记录
	 * @return int >0代表操作成功
	 */
	public int update${table.className}(${table.className} ${table.javaProperty}) throws Exception{
		String sql = "update ${table.tableName} set <@updateColums/> where <#list table.primaryKeys as c>${c.columnName}</#list>=?";
		int returnVal=jdbcTemplate.update(sql,new Object[]{<@updateVals/>,<#list table.primaryKeys as c>${table.javaProperty}.get${c.javaPropertyForGetSet}()</#list>});
		return returnVal;
	}
	-->
	/**
	 * 说明：根据主键删除表${table.tableName}中的记录
	 * @return int >0代表操作成功
	 */
	public int[] delete${table.className}(String  ${table.primaryKey.javaProperty}s) throws Exception{
		String sql = "delete from  ${table.tableName} where ${table.primaryKey.columnName}=?";
		String[] ${table.primaryKey.javaProperty}Arr  =  ${table.primaryKey.javaProperty}s.split(",");
		List<Object[]> paraList = new ArrayList<Object[]>(); 
		for (int i = 0; i < ${table.primaryKey.javaProperty}Arr.length; i++) {
			paraList.add(new Object[]{${table.primaryKey.javaProperty}Arr[i]});
		}
		return jdbcTemplate.batchUpdate(sql,paraList);
	}
	</#if>
}
