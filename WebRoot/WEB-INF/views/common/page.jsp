<%@include file="/taglib.jsp"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<c:set value="${not empty formID?formID:'form'}" var="formID" />
<form id="pageForm">
	<input type="hidden" value="${page.totalPage}" name="totalPage" />
	<input type="hidden" value="${page.currentPage}" name="currentPage" />
</form>
<script>
  laypage({
    cont: 'page',
    pages:'${page.totalPage}',
    curr: '${page.currentPage}', //当前页
    skip: true,//是否开启跳页   
    jump: function(obj, first){
    	if(!first){ //一定要加此判断，否则初始时会无限刷新
	        $('input[name="currentPage"]').val(obj.curr);
	        $('#search').click();
    	}
      }
  });
</script>