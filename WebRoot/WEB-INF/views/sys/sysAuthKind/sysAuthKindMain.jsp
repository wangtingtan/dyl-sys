<%@include file="/taglib.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>{todo}</title>
		<%@include file="/WEB-INF/views/common/commonCss.jsp"%>
	</head>

	<body>
		<div class="admin-main">
			<!-- 查询条件 -->
			<form  action="sysAuthKind!main.do" id="mainForm" method="post">
				<div>
					<a href="javascript:;" class="layui-btn layui-btn-small" id="add">
						<i class="layui-icon">&#xe608;</i> 添加
					</a>
				</div>
			</form>
			<!-- 主table -->
				<div class="layui-field-box layui-form">
					<table class="layui-table admin-table">
						<thead>
							<tr>
								<th>ID</th>
								<th>种类名称</th>
								<th>操作</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${sysAuthKindList}" var="o">
								<tr>
									<td>${o.id}</td>
									<td>${o.name}</td>
							        <td>
										<a href="javascript:;" class="layui-btn layui-btn-mini" data-id="${o.id}" data-opt="edit">
											<i class="layui-icon">&#xe642;</i> 修改
										</a>
										<c:if test="${o.id>4}">
											<a href="javascript:;" class="layui-btn layui-btn-danger layui-btn-mini" data-id="${o.id}" data-opt="delete">
												<i class="layui-icon">&#xe640;</i> 删除
											</a>
										</c:if>
									</td>
							    </tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
		</div>
		<!-- 通用js -->
		<%@include file="/WEB-INF/views/common/commonJs.jsp"%>
		<script>
			//查询方法
			$('#search').click(function(){
				//将分页的 的数据带过来
				$('#mainForm').append($('#pageForm').html());
				showLoading();//显示等待框
				$('#mainForm').submit();
			});
			//添加方法
			$('#add').click(function(){
				var para={
					url:"sysAuthKind!sysAuthKindForm.do",
					title:"添加用户",
					btnOK:"保存",
					area: ['350px', '200px']
				};
				addOrUpdate(para);//通用新增修改方法
			});
			//修改方法
			$('[data-opt=edit]').click(function(){
				var para={
					url:"sysAuthKind!sysAuthKindForm.do",
					para:"id="+$(this).attr("data-id"),
					title:"修改用户",
					btnOK:"修改",
					area: ['350px', '200px']
				};
				addOrUpdate(para);//通用新增修改方法
			});
			//删除方法
			$('[data-opt=delete]').click(function(){
				var dataIds = $(this).attr("data-id");
				if(dataIds){
					layer.confirm('确认删除所选择的吗?', function(index){
						getJsonDataByPost("sysAuthKind!delete.do","dataIds="+dataIds,function(data){
							if(data.result){
								layer.alert("删除成功!",function(){
									$('#mainForm').submit();//查询
								}); 
							}else{
								l.alert(data.msg);//错误消息弹出
							}
							layer.close(index);//关闭删除确认提示框
						},true); 
					});
				}
			});
		
		</script>
	</body>
</html>