/*
Navicat MySQL Data Transfer

Source Server         : dyl
Source Server Version : 50547
Source Host           : localhost:3306
Source Database       : dyl-sys

Target Server Type    : MYSQL
Target Server Version : 50547
File Encoding         : 65001

Date: 2017-06-01 15:51:16
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for sys_auth_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_auth_role`;
CREATE TABLE `sys_auth_role` (
  `id` decimal(10,0) NOT NULL COMMENT '角色id',
  `role_id` decimal(10,0) DEFAULT NULL COMMENT ' 角色id',
  `menu_id` decimal(10,0) DEFAULT NULL COMMENT '菜单id',
  `kind_id` decimal(10,0) DEFAULT NULL COMMENT '权限id',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_auth_role
-- ----------------------------
INSERT INTO `sys_auth_role` VALUES ('100024', '2', '2', '1', '2017-05-27 11:27:37');
INSERT INTO `sys_auth_role` VALUES ('100025', '2', '2', '4', '2017-05-27 11:27:37');
INSERT INTO `sys_auth_role` VALUES ('100026', '2', '3', '1', '2017-05-27 11:27:37');

-- ----------------------------
-- Table structure for sys_kind
-- ----------------------------
DROP TABLE IF EXISTS `sys_kind`;
CREATE TABLE `sys_kind` (
  `id` decimal(10,0) NOT NULL,
  `name` varchar(20) DEFAULT NULL COMMENT '种类名称',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_kind
-- ----------------------------
INSERT INTO `sys_kind` VALUES ('1', '查看');
INSERT INTO `sys_kind` VALUES ('2', '新增');
INSERT INTO `sys_kind` VALUES ('3', '修改');
INSERT INTO `sys_kind` VALUES ('4', '删除');

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu` (
  `id` decimal(10,0) NOT NULL,
  `pid` decimal(10,0) DEFAULT NULL COMMENT '父Id',
  `oid` decimal(10,0) DEFAULT NULL COMMENT '排序',
  `title` varchar(50) DEFAULT NULL COMMENT '名称',
  `note` varchar(100) DEFAULT NULL COMMENT '说明',
  `state` char(1) DEFAULT '0' COMMENT '状态',
  `url` varchar(100) DEFAULT NULL COMMENT '地址',
  `icon` varchar(100) DEFAULT NULL COMMENT '图片代码',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `creator` decimal(10,0) DEFAULT NULL,
  `action_class` varchar(50) DEFAULT NULL,
  `view_level` decimal(10,0) DEFAULT '5' COMMENT '查看等级',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES ('1', '0', '1', '系统管理', null, '0', null, 'fa-cubes', '2017-05-27 11:24:12', '0', null, '2');
INSERT INTO `sys_menu` VALUES ('2', '1', '-1', '用户管理', '', '0', 'sysUser!main.do', '&#xe612;', '2017-06-01 15:41:26', '0', 'sysUserAction', '2');
INSERT INTO `sys_menu` VALUES ('3', '1', '0', '菜单管理', null, '0', 'sysMenu!main.do', '&#xe62a;', '2017-06-01 15:48:07', '0', '', '1');
INSERT INTO `sys_menu` VALUES ('4', '1', '6', '角色管理', null, '0', 'sysRole!main.do', '&#xe61b;', '2017-05-27 11:24:12', null, 'sysRoleAction', '2');
INSERT INTO `sys_menu` VALUES ('6', '1', '7', '数据库监控', '', '0', 'druid/index.html', '&#xe636;', '2017-06-01 15:16:57', '0', '', '1');
INSERT INTO `sys_menu` VALUES ('7', '1', '8', '定时任务管理', '', '0', 'sysQuartz!main.do', '&#xe62c;', '2017-06-01 15:41:17', '0', '', '1');
INSERT INTO `sys_menu` VALUES ('5', '1', '9', '代码生成器', '', '0', 'easyCode!main.do', '&#xe60a;', '2017-06-01 15:44:32', '0', '', '1');
INSERT INTO `sys_menu` VALUES ('8', '1', '10', '日志管理', '', '0', 'log!main.do', '&#xe621;', '2017-06-01 15:47:12', '0', '', '1');

-- ----------------------------
-- Table structure for sys_menu_kind
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu_kind`;
CREATE TABLE `sys_menu_kind` (
  `id` decimal(10,0) NOT NULL,
  `menu_id` decimal(10,0) DEFAULT NULL COMMENT '菜单Id',
  `kind_id` decimal(10,0) DEFAULT NULL COMMENT '种类Id',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_menu_kind
-- ----------------------------
INSERT INTO `sys_menu_kind` VALUES ('206', '87', '1', '2017-05-27 11:25:37');
INSERT INTO `sys_menu_kind` VALUES ('207', '87', '2', '2017-05-27 11:25:37');
INSERT INTO `sys_menu_kind` VALUES ('192', '4', '1', '2017-05-27 11:25:37');
INSERT INTO `sys_menu_kind` VALUES ('193', '4', '2', '2017-05-27 11:25:37');
INSERT INTO `sys_menu_kind` VALUES ('100064', '2', '4', '2017-06-01 15:41:26');
INSERT INTO `sys_menu_kind` VALUES ('100063', '2', '3', '2017-06-01 15:41:26');
INSERT INTO `sys_menu_kind` VALUES ('100062', '2', '2', '2017-06-01 15:41:26');
INSERT INTO `sys_menu_kind` VALUES ('100061', '2', '1', '2017-06-01 15:41:26');
INSERT INTO `sys_menu_kind` VALUES ('194', '4', '3', '2017-05-27 11:25:38');
INSERT INTO `sys_menu_kind` VALUES ('195', '4', '4', '2017-05-27 11:25:38');
INSERT INTO `sys_menu_kind` VALUES ('190', '3', '1', '2017-05-27 11:25:38');
INSERT INTO `sys_menu_kind` VALUES ('191', '3', '2', '2017-05-27 11:25:38');
INSERT INTO `sys_menu_kind` VALUES ('100057', '100053', '2', '2017-05-27 16:27:29');
INSERT INTO `sys_menu_kind` VALUES ('100056', '100053', '1', '2017-05-27 16:27:29');

-- ----------------------------
-- Table structure for sys_quartz
-- ----------------------------
DROP TABLE IF EXISTS `sys_quartz`;
CREATE TABLE `sys_quartz` (
  `id` decimal(10,0) NOT NULL,
  `triggername` varchar(40) DEFAULT NULL COMMENT '触发器名称',
  `cronexpression` varchar(40) DEFAULT NULL COMMENT '时间表达式',
  `jobdetailname` varchar(40) DEFAULT NULL COMMENT '任务名称',
  `targetobject` varchar(40) DEFAULT NULL COMMENT '目标名称',
  `methodname` varchar(40) DEFAULT NULL COMMENT '方法名称',
  `concurrent` decimal(10,0) DEFAULT '0' COMMENT '是否并发',
  `state` decimal(10,0) DEFAULT '0' COMMENT '状态',
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_quartz
-- ----------------------------
INSERT INTO `sys_quartz` VALUES ('1', '测试', '0/3 * * * * ?', 'detailname', 'dyl.sys.quartz.TestQuarz', 'haha', '1', '0', '2017-05-27 17:20:45');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `id` decimal(10,0) NOT NULL,
  `name` varchar(200) DEFAULT NULL COMMENT '名称',
  `note` varchar(200) DEFAULT NULL COMMENT '说明',
  `creator` decimal(10,0) DEFAULT NULL COMMENT '创建人',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES ('1', '超级管理员', '超级管理员', '1', '2017-05-27 11:26:28');
INSERT INTO `sys_role` VALUES ('2', '测试角色', '测试角色', '1', '2017-05-27 11:26:28');
INSERT INTO `sys_role` VALUES ('0', '开发管理员', '开发管理员最高权限', '1', '2017-05-27 11:26:28');
INSERT INTO `sys_role` VALUES ('212', '测试2', '测试21', '2', '2017-05-27 11:26:28');

-- ----------------------------
-- Table structure for sys_role_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_user`;
CREATE TABLE `sys_role_user` (
  `id` decimal(10,0) NOT NULL,
  `roleid` decimal(10,0) DEFAULT NULL COMMENT '角色id',
  `userid` decimal(10,0) DEFAULT NULL COMMENT '用户id',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `creator` decimal(10,0) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_role_user
-- ----------------------------
INSERT INTO `sys_role_user` VALUES ('209', '0', '0', '2017-05-27 11:26:47', '2');
INSERT INTO `sys_role_user` VALUES ('210', '1', '2', '2017-05-27 11:26:47', '2');
INSERT INTO `sys_role_user` VALUES ('262', '2', '211', '2017-05-27 11:26:47', '211');
INSERT INTO `sys_role_user` VALUES ('263', '212', '211', '2017-05-27 11:26:47', '211');

-- ----------------------------
-- Table structure for sys_sequence
-- ----------------------------
DROP TABLE IF EXISTS `sys_sequence`;
CREATE TABLE `sys_sequence` (
  `name` varchar(50) NOT NULL COMMENT '序列的名字，唯一',
  `current_value` bigint(20) NOT NULL COMMENT '当前的值',
  `increment_value` int(11) NOT NULL DEFAULT '1' COMMENT '步长，默认为1',
  PRIMARY KEY (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_sequence
-- ----------------------------
INSERT INTO `sys_sequence` VALUES ('seq_id', '100065', '1');

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `id` decimal(10,0) NOT NULL DEFAULT '0',
  `username` varchar(50) NOT NULL,
  `name` varchar(50) DEFAULT NULL COMMENT '姓名',
  `password` varchar(100) DEFAULT NULL COMMENT '密码',
  `email` varchar(100) DEFAULT NULL COMMENT '邮箱',
  `phone` varchar(100) DEFAULT NULL COMMENT '电话',
  `state` varchar(1) DEFAULT '0' COMMENT '状态',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `creator` decimal(10,0) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('0', 'dev', 'dev', '202CB962AC59075B964B07152D234B70', '大神', null, '0', '2017-05-27 11:27:09', '0');
INSERT INTO `sys_user` VALUES ('211', 'test', '测试人员1', '098F6BCD4621D373CADE4E832627B4F6', null, '222', '0', '2017-05-27 11:27:09', '0');
INSERT INTO `sys_user` VALUES ('2', 'admin', 'admin', '21232F297A57A5A743894A0E4A801FC3', null, null, '0', '2017-05-27 16:32:53', '0');
INSERT INTO `sys_user` VALUES ('100044', 'test2', '测试2', 'E10ADC3949BA59ABBE56E057F20F883E', 'sdfsdf@sdf', '123123', '0', '2017-05-27 16:05:14', '0');

-- ----------------------------
-- Function structure for func_currval
-- ----------------------------
DROP FUNCTION IF EXISTS `func_currval`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `func_currval`(seq_name varchar(50)) RETURNS int(11)
begin
 declare value integer;
 set value = 0;
 select current_value into value
 from sys_sequence
 where name = seq_name;
 return value;
end
;;
DELIMITER ;

-- ----------------------------
-- Function structure for func_nextval
-- ----------------------------
DROP FUNCTION IF EXISTS `func_nextval`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `func_nextval`(seq_name varchar(50)) RETURNS int(11)
begin
 update sys_sequence
 set current_value = current_value + increment_value
 where name = seq_name;
 return func_currval(seq_name);
end
;;
DELIMITER ;
