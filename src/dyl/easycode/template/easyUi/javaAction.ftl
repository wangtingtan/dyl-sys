<#macro selectolums><#list table.columns as c><#if c_has_next>${c.columnName},<#else>${c.columnName}</#if></#list></#macro>
<#macro insertFlag><#list table.baseColumns as c><#if c_has_next>?,<#else>?</#if></#list></#macro>
<#macro updateColums><#list table.baseColumns as c><#if c_has_next>${c.columnName}=?,<#else>${c.columnName}=?</#if></#list></#macro>
<#macro updateVals><#list table.baseColumns as c><#if c_has_next>${table.javaProperty}.get${c.javaPropertyForGetSet}(),<#else>${table.javaProperty}.get${c.javaPropertyForGetSet}()</#if></#list></#macro>
package ${table.packageName};

import java.util.List;
<#list table.importList as i>
import ${i};
</#list>
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import javax.annotation.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.jdbc.core.JdbcTemplate;
import com.common.action.BaseAction;
import com.common.exception.CommonException;
import com.common.page.EasyUiPage;
import com.auth.bean.Authority;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.StringUtils;
/**
 * 由dyl EasyCode自动生成
 * @author Dyl
 * ${sysTime}
 */
@Controller
public class ${table.className}Action extends BaseAction{
	@Resource
	private JdbcTemplate jdbcTemplate;
	@Resource
	private ${table.className}ServiceImpl ${table.javaProperty}ServiceImpl;
	
	 /**
	 * 说明：进入主页面方法
	 * @return String
	 */
	@RequestMapping(value = "/${table.javaProperty}!into${table.className}Main.do")
	public String  into${table.className}Main(){
		<#list table.columns as c>
		<#if c.selectSql??>
		request.setAttribute("${c.javaProperty}List",jdbcTemplate.queryForList("${c.selectSql}"));	
		</#if>
		</#list>
		<#--if(!getAuthority().is(getAuthority().getAuthid()))return "authErr";//判断查询权限-->
		return "${table.javaProperty}";
	}
	
	/**
	 * 说明：查询返回封装成JsonObject
	 * @return JsonObject
	 */
	@ResponseBody()
	@RequestMapping(value = "/${table.javaProperty}!find${table.className}List.do")
	public String  find${table.className}List(EasyUiPage easyUiPage,${table.className} ${table.javaProperty}){
		<#--if(!getAuthority().is(getAuthority().getAuthid()))return authErr();//判断查询权限-->
		try {
			return ${table.javaProperty}ServiceImpl.find${table.className}List(easyUiPage.swiftEasyUiPage(), ${table.javaProperty}).toJSONString();
		} catch (Exception e) {
			throw new WGMSException("查询列表", e);
		}
	}
	/**
	 * 说明：根据主键查询表sys_lan中的一条记录 
	 * @return ${table.className}
	 */
	@ResponseBody()
	@RequestMapping(value = "/${table.javaProperty}!get${table.className}.do")
	public String get${table.className}(${table.primaryKey.javaType}  ${table.primaryKey.javaProperty}){
		<#--if(!getAuthority().is(getAuthority().getAuthid()))return authErr();//判断查询权限-->
		try {
			JSONObject json = new JSONObject();
			return json.toJSONString(${table.javaProperty}ServiceImpl.get${table.className}(${table.primaryKey.javaProperty}));
		} catch (Exception e){
			throw new WGMSException("查询单个", e);
		}
	}
	<#if hasEdit??>
	/**
	 * 说明：更新或者插入${table.tableName}
	 * @return int >0代表操作成功
	 */
	@ResponseBody()
	@RequestMapping(value = "/${table.javaProperty}!saveOrUpdate${table.className}.do")
	public JSONObject saveOrUpdate${table.className}(${table.className} ${table.javaProperty}){
		try {
			int ret = 0;
		<#if table.primaryKey.javaType =='String'>
			if(StringUtils.isEmpty(${table.javaProperty}.get${table.primaryKey.javaPropertyForGetSet}())){
			<#else>
			if(${table.javaProperty}.get${table.primaryKey.javaPropertyForGetSet}()==null){
		</#if>
				<#--if(!getAuthority().is(getAuthority().getAuthid(),Authority.ADD))return authErr();//判断是否有新增权限-->
				ret = ${table.javaProperty}ServiceImpl.insert${table.className}(${table.javaProperty});
			}else{
				<#--if(!getAuthority().is(getAuthority().getAuthid(),Authority.EDIT))return authErr();//判断是否有修改权限-->
				ret = ${table.javaProperty}ServiceImpl.update${table.className}(${table.javaProperty});
			}
			return returnByDbRet(ret);
		} catch (Exception e) {
			throw new WGMSException("插入或更新", e);
		}
	}
	/**
	 * 说明：根据主键删除表${table.tableName}中的记录
	 * @return int >0代表操作成功
	 */
	@ResponseBody()
	@RequestMapping(value = "/${table.javaProperty}!delete${table.className}.do")
	public JSONObject delete${table.className}(String  ${table.primaryKey.javaProperty}s){
		try {
			<#--if(!getAuthority().is(getAuthority().getAuthid(),Authority.DELETE))return authErr();//判断是否有删除权限-->
			int[]	ret = ${table.javaProperty}ServiceImpl.delete${table.className}(${table.primaryKey.javaProperty}s);
			return returnSuccess();
		} catch (Exception e) {
			throw new WGMSException("删除", e);
		}
	}
	</#if>
	<#if hasUpload??>
	/**
	 * 说明：清空表${table.tableName}中的记录
	 * @return int >0代表操作成功
	 */
	@ResponseBody()
	@RequestMapping(value = "/${table.javaProperty}!truncate${table.className}.do")
	public JSONObject truncateDrSyd(){
		try {
			return returnByDbRet(jdbcTemplate.update("truncate table ${table.tableName}"));
		} catch (Exception e) {
			throw new WGMSException("删除", e);
		}
	}
	</#if>
}
